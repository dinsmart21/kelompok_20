<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\film;
use File;

class filmcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,'
        ]);

        $namaposter = time() . '.' . $request->poster->extension();

        $request->poster->move(public_path('poster'), $namaposter);

        $film = new film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $namaposter;

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::findOrFail($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = film::findOrFail($id);
        return view('film.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,'
        ]);

        if($request->has('poster')){
            $film = film::find($id);

            $path = "poster/";
            File::delete($path .$film->poster);

            $namaposter = time() . '.' . $request->poster->extension();

        $request->poster->move(public_path('poster'), $namaposter);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->genre_id = $request->genre_id;
            $film->tahun = $request->tahun;
            $film->poster = $namaposter;     
            
            $film->save();

            return redirect('/film');
        }else{
            $film = film::find($id);
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->genre_id = $request->genre_id;
            $film->tahun = $request->tahun;
            

            $film->save();
            return redirect('/film');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = film::find($id);

            $path = "poster/";
            File::delete($path .$film->poster);

            $film->delete();

            return redirect('/film');
    }
}
