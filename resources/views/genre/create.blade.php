@extends('layout.master')
@section('judul')
Nama Genre Film
@endsection

@section('content')

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Jenis">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
       
    <button type="submit" class="btn btn-dark">Tambah</button>
</form>





@endsection