@extends('layout.master')
@section('judul')
Edit Cast  {{$cast->nama}}
@endsection
    
@section('content')
  
<form action="/cast/{{$cast->id}}" method="POST">
          @csrf
          @method('put')
          <div class="form-group">
              <label>NAMA</label>
              <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Title">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
            <label>UMUR</label>
            <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
          <div class="form-group">
              <label>BIO</label>
              <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
              
              @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
      </form>      





@endsection